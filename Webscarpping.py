from bs4 import BeautifulSoup
import requests
article=requests.get("https://f1000research.com/articles/4-1522")
soup=BeautifulSoup(article.text,'lxml')
types=soup.find('div',class_="article-type article-display").text
title_name=soup.find('h1',class_="js-article-title").text
authors=soup.find('span',class_="js-article-author").text
Date_publised=soup.find('div',class_="article-pubinfo-mobile").text
files=soup.find('div',class_="article-abstract article-page-general-text-mobile research-layout").text
keyword=soup.find('p',class_="u-mb--0 u-pb--2").text
article=soup.pagenum
article=soup.date_received
article=soup.Date_accepted
article_dict={}
article_dict={'Publish Type':types,'Title Name':title_name,'Author Name':authors,'Date Published':Date_publised,'Abstract':files,
             'Keyword':keyword,'Page Number':article,'Date Received':article,'Date Accepted':article}
print(article_dict)
