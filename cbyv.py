#immutable values=call-by-value
string="santho"
def san(str):                     
    string="santho draws well"
    print(string)
    return
san(string)
print(string)
#mutable values=call-by-reference
def printme(list):
    mylist.append([1,2,3])
    #mylist=[1,2,3]
    print(mylist)
    return
mylist=[10,20,30]
printme(mylist)
print(mylist) 
#variable-length argument
def ss(a,*b):
    print(a)
    for var in b:
        print(var)
    return
ss(10)
ss(20,30,40) 
    