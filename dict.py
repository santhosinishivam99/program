dict1={'name':'zara','age':7,'school':'vsr'}
print(type(dict1))          #to print the type 
print('name:',dict1['name']) #to print using key
dict1['name']='santho'       #to update the name
del dict1['age']              #to delete using key
print(dict1)
#using cmp function to print the same key:value pair in dictionary
def cmp(dict2,dict3,dict4):
    for key in dict2:
        if(key in dict2 and dict2[key]==dict3[key]):
            dict4[key]=dict2[key]
            print(dict4) 
a={'name':'santho','country':'US'}
b={'name':'santho','country':'India'}
c={}
cmp(a,b,c)
