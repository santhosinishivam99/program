dict1={'a':1,'b':2,'c':3}
dict1['d']=4               #updating the key:value pair
dict3=dict1.copy()         #to copy from dict1
print(dict3)
print(dict1.keys())         #to print keys
print(dict1.values())       #to print values
tuple1=(1,2,3,4,5)          
dict=dict.fromkeys(tuple1,10)  #to use fromkeys for creating new dictionary and append values
print(dict) 