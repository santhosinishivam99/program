list1=[1,2,3,4,5]
2 in [1,2,3,4,5]        #membersip
print('true')    
list2=['physics','chemistry','maths','social','science'] 
list3=list1+list2       #use + to concatenate
list4=['Hi'] 
print(list4*5)          #use * for repetition 
print(list3)            
list2[2]="biology"      #updating the index
del list1[2]            #use del keyword to delete specific item
print(list1) 
print(len(list2))       #use len to find length of the list
print("list1[0]",list1[0])  #using index to print particular value
print("list2[2]",list2[-5]) #negative count start from the right side
for x in list1:         #iteration
    print(x)
print(list1[2:])        #slicing