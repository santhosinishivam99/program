#program to illustrate polymorphism with function
class  India:
    def capital(self):
        print("New Delhi is a capital of India")
    def language(self):
        print("Hindi is most spoken language in India")
    def types(self):
        print("India is a developing country")
class USA:
    def capital(self):
        print("Washington is the capital of USA")
    def language(self):
        print("English is the primary language")
    def types(self):
        print("USA is a developed country")
def function(obj):                                        #function that allow any obj.
    obj.capital()
    obj.language()
    obj.types()
obj_ind=India()
obj_us=USA()
function(obj_ind)
function(obj_us)