dict1={'name':'zara','age':7,'school':'vsr'}
print((dict1))               #to print dict1
print('name:',dict1['name']) #to print using key
dict1['name']='hasini'       #to update the name
print(dict1)     
del dict1['age']              #to delete using key
print(dict1)
#using cmp function to print the same key:value pair in dictionary
def cmp(dict2,dict3,dict4):
    for key in dict2:
        if(key in dict2 and dict2[key]==dict3[key]):
            dict4[key]=dict2[key]
            print(dict4) 
a={'name':'hasini','country':'US'}
b={'name':'hasini','country':'India'}
c={}
cmp(a,b,c)

tuple1=(1,2,3,4,5)          
dicts=dict.fromkeys(tuple1,10)  #to use fromkeys for creating new dictionary and append values
print(dicts) 