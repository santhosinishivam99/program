class parent:
    def function(self):
        print("this is a function")
class child(parent):                  #first child class
    def function1(self):
        print("this is function1")
class child1(parent):                 #second child class
    def function2(self):
        print("this is function2")
obj=child()
obj1=child1()
obj.function()
obj.function1()
obj1.function2()
