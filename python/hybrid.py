class Parent:                             #single inheritance
    def func1(self):
        print("this is function one") 
class Child(Parent):                      
    def func2(self):
        print("this is function 2") 
class Child1(Parent):                     #hierarchical inheritance
    def func3(self):
        print(" this is function 3") 
class Child3(Child,Child1):               #multilevel inheritance
    def func4(self):
        print(" this is function 4") 
ob = Child3()
ob.func1()
ob.func2()
ob.func3()
ob.func4() 