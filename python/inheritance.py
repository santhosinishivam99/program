class Dog:                                               #parent class
    species="canis"                                      #class attribute
    def __init__(self,name,color,sound):                 
        self.name=name
        self.color=color
        self.sound=sound
        
    def dog1(self):
        print(f"{self.name} is {self.color} in nature")

    def dog2(self,age,nature):
        self.age=age
        self.nature=nature
        print(f"{self.name} is {self.age} years old")
        print(f"{self.name} says {self.sound}") 
        print(f"{self.name} is {self.nature} dog")

class GoldenRetriever(Dog):                              #child class
    def dog2(self,age=7,nature="good"):                  #extend the attribute 
        return super().dog2(age,nature)                  #using super() to extend all the functionality from parent class

x=GoldenRetriever("buddy","light brown","arf")         #creating instance by using child class
x.dog2()                                                 
print(x.species)                                         #print the class attribute.