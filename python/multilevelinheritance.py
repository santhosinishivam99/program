class parent:
    def function(self):
        print("this is a function1")
class child1(parent):                      
    def function1(self):
        print("this is a function2")
class child2(child1):                    #multilevel inheritance
    def function3(self):
        print("this is a function3")
obj=child2()
obj.function()
obj.function1()
obj.function3()