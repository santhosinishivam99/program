class parent:                            #first parent class
    def funct1(self):
        print("this is a function1")
class parent1:                          #second parent class
    def function(self):
        print("this is a function2")
class child(parent,parent1):            #multiple inheritance    
    def function2(self):
        print("this is a function3")
obj=child()
obj.funct1()
obj.function()
obj.function2()