#program to illustarte public,private,protected variable members in a class
class dog:                                                  #parent class
    def __init__(self,age,color,name):                          
        self._age=age                                       #protected variable
        self.__color=color                                  #private variable
        self.name=name                                      #public variable
        #print(age)
        #print(color)
class char(dog):                                             #child class
    def funct(self):
        print(f"{self.name} dog has {self._age} years old")
ob=char(7,'brown',"buddy")                                   #instance for child class
ob.funct()                                                   #calling the function using instance.
print(ob._age)                                               #print the protected variable member
print(ob._dog__color)                                        #print private variable using dog class only where created private mmeber
print(ob.name)                                               #print the public variable member
