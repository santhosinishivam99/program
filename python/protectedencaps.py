class dog:
    _height=5                  #protected variable
    _age=7                     #protected variable
class dog1(dog):               #child class
    def __init__(self):        
        print(self._height)    #protected member can be accessed within child class
        print(self._age)
ob=dog1()                      #instance of the class dog1
print(ob._height)              #print protected variable
print(ob._age) 

