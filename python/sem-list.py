pow2=[2**x for x in range(10)] #list comprehension
print(pow2)                    
print(pow2[0])                 #using index
print(pow2[-1])                #using negative index
print(pow2[2:5])               #slicing
pow2.append(10)                #insert elements
pow2.extend([10,11])           #to add another list into pow2
print(pow2)
for fruits in ['apple','banana','mango']:  #iterating using for loop
    print("I like",fruits)  
