my_tuple=3,4.6,"dog"  
print(my_tuple)
list1=list(my_tuple)                    #converting tuple into list
print(list1)
a,b,c=my_tuple                          #unpacking
print(a)
print(b)
print(c)
print(3 in my_tuple)                    #membership test
n_tuple=("mouse",(8,4,6),(1,2,3))       
print(n_tuple[0][4])                    #using index to print
print(n_tuple[1][1])