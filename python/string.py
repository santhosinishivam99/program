string="hello"
string1="world"
print(string+string1)            #concatenation
print(string[2])                 #using index to print particular char in string
print(string[-1])                #using negative index
print(string[0:2])               #slicing
print(string * 2)                #repetition
str2=list(enumerate(string))     #returns index and values of all items in a string
print(str2)
print(len(string))               #character count
print(string.lower())            #print in a lower case using lower() method
print(string.upper())            #to print in a upper case
string3="This is used to split the string"
print(string3.split())           #split the string
print(string3.find('is'))        #returns how many elements
print(string3.replace('split','join'))    #replace the word 
print("he said, \"whats there?\"") 
print('he said, "whats there?"') 
print('''he said,\"whats there?"''')