#to create vehicle1 class
class vehicle1:
    #to instantiating a class
    def __init__(self,number_of_wheels,type_of_tank,seating_capacity,maximum_velocity):
        #to save the attributes using self
        self.number_of_wheels=number_of_wheels
        self.type_of_tank=type_of_tank
        self.seating_capacity=seating_capacity
        self.maximum_velocity=maximum_velocity
    #to create vehicle function
    def vehicle(self):
        print"the model is",self.number_of_wheels,self.type_of_tank,self.seating_capacity,self.maximum_velocity
    #to access attributes,creating methods.
    def number_of_wheels(self,number):
        return self.number_of_wheels
        self.number_of_wheels=number
    def type_of_tank(self,number):
        return self.type_of_tank
        self.type_of_tank=number 
    def seating_capacity(self,number):
        return self.seating_capacity
        self.seating_capacity=number 
#to create objects for the class vehicle1 
tesla_1=vehicle1(4,'electric',5,250)
#to change the value
tesla_1.number_of_wheels=2
tesla_1.type_of_tank='motor'
tesla_1.seating_capacity=8
#to print that value
print(tesla_1.number_of_wheels)
print(tesla_1.type_of_tank)
print(tesla_1.seating_capacity)
#to call the class
tesla_1.vehicle() 