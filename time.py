import time        #used to get required time
ticks=time.time()  #to get the current instance time as a floating point no.s
print(ticks)
time.sleep(5)      #suspends execution for given no.of seconds
localtime=time.localtime(time.time()) #to get the current time
print(localtime)
time.sleep(10)
localtime=time.asctime(time.localtime(time.time())) #getting formatted line of current time
print(localtime)