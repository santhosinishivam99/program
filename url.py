from bs4 import BeautifulSoup
import requests
import re
def getHTMLdocument(url):
    response=requests.get(url)
    return response.text
url_to_scrap="https://www.geeksforgeeks.org/c-programming-language/"
html_docu=getHTMLdocument(url_to_scrap)
soup=BeautifulSoup(html_docu,'html')
for link in soup.find_all('a',attrs={'href': re.compile("^https://")}):
    print(link.get('href')) 