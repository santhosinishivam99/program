from bs4 import BeautifulSoup
import requests
files=requests.get('https://www.timesjobs.com/candidate/job-search.html?searchType=personalizedSearch&from=submit&txtKeywords=python&txtLocation=').text
#print(files) #it gives the response to the url
#print(files) #to print the url in the text format(html)
soup=BeautifulSoup(files,'lxml')
jobs=soup.find_all('li',class_="clearfix job-bx wht-shd-bx") #to list the jobs in the class
#print(jobs)
#company_name=jobs.find('h3',class_="joblist-comp-name")#just to print only the company name using find method along with tag
for job in jobs:
    published_date=job.find('span',class_="sim-posted")
    if 'few' in published_date:
        company_name=job.find('h3',class_="joblist-comp-name").text.replace(' ','')#only print the comapany name
        skills=job.find('span',class_="srp-skills").text.replace(' ','')#replace the white space
    #published_date=job.find('span',class_="sim-posted").span.text
#print(skills)
#print(company_name)
        print(f'''
        Company Name: {company_name}
        Required Skills: {skills}
        ''')
        print('')
#published_date=jobs.find('span',class_="sim-posted")
#print(published_date)
